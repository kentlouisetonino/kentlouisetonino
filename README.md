``Go`` | ``TypeScript`` | ``Node.js`` | ``Backend`` | ``CLI`` | ``Snapcraft`` | ``Linux``

##
``Github`` | https://github.com/kentlouisetonino <br />
``Gitlab`` | https://gitlab.com/kentlouisetonino <br />
``Linkedin`` | https://www.linkedin.com/in/kentlouisetonino <br />
``Snapcraft`` | https://snapcraft.io/publisher/kentlouisetonino <br />
``Gists`` | https://gist.github.com/kentlouisetonino

<br />

## Software Project
``Go`` | ``BaseShift`` | https://github.com/kentlouisetonino/baseshift <br />
``Go`` | ``Aggreflow`` | https://github.com/kentlouisetonino/aggreflow <br />
``C++`` | ``Bus Calculator`` | https://github.com/kentlouisetonino/bus-calculator <br />
``Node.js`` | ``Node-S3`` | https://github.com/kentlouisetonino/node-s3 <br />

